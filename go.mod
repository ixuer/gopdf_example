module gitee.com/ixuer/gopdf_example

go 1.18

require (
	github.com/signintech/gopdf v0.15.0
	gitee.com/ixuer/gopdf_example/pkg/gofpdi v0.0.0

)

require (
	github.com/phpdave11/gofpdi v1.0.11 // indirect
	github.com/pkg/errors v0.8.1 // indirect
)

replace gitee.com/ixuer/gopdf_example/pkg/gofpdi => ./pkg/gofpdi

## Generate PDF files by go language

[English](./README.md) | [简体中文](./README_zh_CN.md)

Generate PDF files by using [signintech/gopdf](https://github.com/signintech/gopdf).

#### Test Case Coverage:
1. Set page configuration like width and height.
2. [test/font](./test/font): Add ttf fonts, and set font.  
3. [test/text](./test/text): Write the text content include long text. 
4. [test/line](./test/line): Draw different kinds of lines. 
5. [test/oval](./test/oval): Draw an oval. 
6. [test/polygon](./test/polygon): Draw a polygon, like triangle, pentagon. 
7. [test/rectangle](./test/rectangle): Draw a rectangle. 
8. [test/image](./test/image): Insert images, options optional, like rotate specified degree, flip vertically or horizontally, mask, transparency. 
9. [test/hyperlink](./test/hyperlink): Add internal or external hyperlink.
10. [test/outline](./test/outline): Add outlines or called bookmark, include multi-level.
11. [test/compress](./test/compress): Set the compression level to compress the file size.  
12. [test/protection](./test/protection): Set password to protect the file.
13. [test/import_tpl](./test/import_tpl): Imported from template, which can be an existing PDF file, such as add cover.

#### Tips:
1. The test cases are in the `gopdf_examples/test/` directory, every case will initial by its own init function.

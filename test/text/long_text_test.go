package text

import (
	"testing"

	"github.com/signintech/gopdf"
)

// TestLongText long text (word wrap is not supported)
func TestLongText(t *testing.T) {
	var err error

	err = pdf.SetFontSize(15)
	if err != nil {
		panic(err)
	}

	pdf.SetTextColor(0, 0, 0)
	err = pdf.Text(longText2)
	if err != nil {
		panic(err)
	}

	save("long-text")
}

// TestLongTextManualWrap long text wrap manually.
func TestLongTextManualWrap(t *testing.T) {
	var err error

	err = pdf.SetFontSize(15)
	if err != nil {
		panic(err)
	}

	texts, err := pdf.SplitText(longText2, gopdf.PageSizeA4.W-pdf.MarginLeft()-pdf.MarginRight()-10) // remove left and right margins
	if err != nil {
		panic(err)
	}

	var x float64 = 10
	var y float64 = 10
	pdf.SetXY(x, y)
	pdf.SetTextColor(0, 0, 0)
	for _, t := range texts {
		pdf.SetX(10)
		pdf.SetNewY(y, 20) // More than 1 page will be automatically paginated. 20 is a user-defined row height, not a fixed value.

		y = pdf.GetY()
		err = pdf.Text(t)
		if err != nil {
			continue
		}
		y += 20
	}

	save("long-text-manual-wrap")
}

// TestMultiCell
// gopdf.MultiCell(rectangle *Rect, text string) Text can be automatically split into multiple lines, but not automatically paginated.
func TestMultiCell(t *testing.T) {

	var err error
	rect := &gopdf.Rect{
		W: gopdf.PageSizeA4.W - pdf.MarginLeft() - pdf.MarginRight(), // remove left and right margins
		H: gopdf.PageSizeA4.H - pdf.MarginTop() - pdf.MarginBottom(), // remove the upper and lower margins
	}

	pdf.SetXY(pdf.MarginLeft(), pdf.MarginTop()) // start from the top left corner of the margin

	err = pdf.SetFontSize(20)
	if err != nil {
		panic(err)
	}

	pdf.SetTextColor(0, 0, 0)
	err = pdf.MultiCell(rect, longText1)
	if err != nil {
		panic(err)
	}

	save("multi_cell")
}

package text

import (
	"testing"
)

func TestText(t *testing.T) {
	var err error

	pdf.SetXY(30, 50)
	err = pdf.SetFontSize(20)
	if err != nil {
		panic(err)
	}

	err = pdf.Text("This is basic text example.")
	if err != nil {
		panic(err)
	}

	pdf.SetXY(30, 80)
	pdf.SetTextColor(0, 142, 243)
	err = pdf.Text("This is basic text example.")
	if err != nil {
		panic(err)
	}

	pdf.SetTextColor(200, 0, 0)
	pdf.SetXY(30, 110)
	err = pdf.Text("This is basic text example.")
	if err != nil {
		panic(err)
	}

	save("text-basic")
}

// TestTextRotation Text rotation (can be used as watermark)
func TestTextRotation(t *testing.T) {
	var err error

	pdf.SetTextColor(200, 0, 0)

	pdf.SetXY(100, 200)

	pdf.Rotate(30, 100, 200)
	err = pdf.SetFontSize(30)
	if err != nil {
		panic(err)
	}

	err = pdf.Text("This is text rotate example.")
	if err != nil {
		panic(err)
	}

	save("text-rotate")
}

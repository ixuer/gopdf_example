package polygon

import (
	"math"
	"testing"

	"github.com/signintech/gopdf"
)

// TestTriangle draw a triangle by 3 points.
func TestTriangle(t *testing.T) {

	var points = []gopdf.Point{
		{
			X: 100,
			Y: 50,
		},

		{
			X: 100,
			Y: 200,
		},

		{
			X: 400,
			Y: 200,
		},
	}

	pdf.SetFillColor(255, 198, 109)
	pdf.Polygon(points, "F")

	save("polygon-triangle")
}

// TestPentagon draw a pentagon by 5 points.
func TestPentagon(t *testing.T) {

	var x float64 = 200
	var y float64 = 100

	var l float64 = 150

	var toAngle = func(angle float64) float64 {
		return angle * (math.Pi / 180)
	}

	var points = []gopdf.Point{
		{
			X: x,
			Y: y,
		},

		{
			X: x - l*math.Sin(toAngle(54)),
			Y: y + l*math.Sin(toAngle(36)),
		},

		{
			X: x - l*math.Sin(toAngle(54)) + l*math.Sin(toAngle(18)),
			Y: y + l*math.Sin(toAngle(36)) + l*math.Sin(toAngle(72)),
		},

		{
			X: x - l*math.Sin(toAngle(54)) + l*math.Sin(toAngle(18)) + l,
			Y: y + l*math.Sin(toAngle(36)) + l*math.Sin(toAngle(72)),
		},

		{
			X: x - l*math.Sin(toAngle(54)) + 2*l*math.Sin(toAngle(18)) + l,
			Y: y + l*math.Sin(toAngle(36)),
		},
	}

	pdf.Polygon(points, "")

	save("polygon-pentagon")
}

package import_tpl

import (
	"fmt"
	"testing"
	"time"

	"github.com/signintech/gopdf"
)

func TestImportExistPDF(t *testing.T) {
	var err error

	pdf := &gopdf.GoPdf{}
	pdf.Start(gopdf.Config{
		Unit:       gopdf.UnitPT,
		TrimBox:    gopdf.Box{},
		PageSize:   *gopdf.PageSizeA4,
		K:          0,
		Protection: gopdf.PDFProtectionConfig{},
	})

	filePath := "../../assets/pdf/test.pdf"

	// import as a template
	tplId := pdf.ImportPage(filePath, 2, "/MediaBox")

	pdf.AddPage()

	// use template
	pdf.UseImportedTemplate(tplId, 0, 0, gopdf.PageSizeA4.W, gopdf.PageSizeA4.H)

	err = pdf.WritePdf(fmt.Sprintf("import-pdf-%s.pdf", time.Now().Format("20060102-150405")))
	if err != nil {
		panic(err)
	}
}

package import_tpl

import (
	"fmt"
	"testing"
	"time"

	"github.com/signintech/gopdf"

	"gitee.com/ixuer/gopdf_example/pkg/gofpdi"
)

// TestAddCover
// @Description: add the cover for an existing file.
// @param t:
func TestAddCover(t *testing.T) {

	pdf := &gopdf.GoPdf{}
	pdf.Start(gopdf.Config{
		Unit:       gopdf.UnitPT,
		TrimBox:    gopdf.Box{},
		PageSize:   *gopdf.PageSizeA4,
		K:          0,
		Protection: gopdf.PDFProtectionConfig{},
	})

	// old pdf file.
	filePath := "../../assets/pdf/test.pdf"

	var totalPage int

	// get the pdf reader by gofpdi.
	reader, err := gofpdi.NewPdfReader(filePath)
	if err != nil {
		panic(err)
	}

	// method GetTotalPage() the gofpdi does not support, so rewrite it to add a method named GetTotalPage().
	totalPage = reader.GetTotalPage()

	pdf.AddPage()

	// add cover
	err = pdf.Image("../../assets/img/cover_1.png", 0, 0, gopdf.PageSizeA4)
	if err != nil {
		panic(err)
	}

	// add every page of old file.
	for i := 1; i < totalPage+1; i++ {

		pdf.AddPage()

		// import as a template
		tplId := pdf.ImportPage(filePath, i, "/MediaBox")

		// use template
		pdf.UseImportedTemplate(tplId, 0, 0, gopdf.PageSizeA4.W, gopdf.PageSizeA4.H)

	}

	err = pdf.WritePdf(fmt.Sprintf("import-pdf-%s.pdf", time.Now().Format("20060102-150405")))
	if err != nil {
		panic(err)
	}

}

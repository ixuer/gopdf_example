package oval

import "testing"

// TestOval draw an oval by 2 points.
// (x1, y1) is the coordinate of top-left.
// (x2, y2) is the coordinate of bottom-right.
func TestOval(t *testing.T) {
	pdf.Oval(20, 30, 300, 120)

	pdf.Oval(20, 200, 100, 300)

	save("oval-basic")
}

package protection

import (
	"testing"

	"github.com/signintech/gopdf"
)

func TestNoProtection(t *testing.T) {

	var err error

	pdf.SetXY(50, 100)
	err = pdf.Text("There is no protection for this PDF.")
	if err != nil {
		panic(err)
	}

	save("protection-none")

}

func TestProtectionOnlyView(t *testing.T) {

	var err error

	pdf.Start(gopdf.Config{
		Unit:     gopdf.UnitPT,
		TrimBox:  gopdf.Box{},
		PageSize: *gopdf.PageSizeA4,
		K:        0,
		Protection: gopdf.PDFProtectionConfig{
			UseProtection: true,            // using protection or not.
			Permissions:   0,               // can only view, but cannot modify, print etc.
			UserPass:      []byte("user"),  // only have given permissions when using user password.
			OwnerPass:     []byte("owner"), // have all permissions when using owner password.
		},
	})

	addAndSetFont()
	addPage()

	pdf.SetXY(50, 100)
	err = pdf.Text("Protected PDF file")
	if err != nil {
		panic(err)
	}

	save("protection-only-view")
}

func TestProtectionPrintable(t *testing.T) {

	var err error

	pdf.Start(gopdf.Config{
		Unit:     gopdf.UnitPT,
		TrimBox:  gopdf.Box{},
		PageSize: *gopdf.PageSizeA4,
		K:        0,
		Protection: gopdf.PDFProtectionConfig{
			UseProtection: true,                   // using protection or not.
			Permissions:   gopdf.PermissionsPrint, // can view and print.
			UserPass:      []byte("user"),         // only have given permissions when using user password.
			OwnerPass:     []byte("owner"),        // have all permissions when using owner password.
		},
	})

	addAndSetFont()
	addPage()

	pdf.SetXY(50, 100)
	err = pdf.Text("Protected PDF file")
	if err != nil {
		panic(err)
	}

	save("protection-printable")
}

package rectangle

import "testing"

// TestRect draw a rectangle by 2 points.
func TestRect(t *testing.T) {
	var err error

	pdf.SetXY(10, 25)
	err = pdf.Text("pdf.Rectangle(10, 30, 110, 230, \"\", 0, 0)")
	if err != nil {
		panic(err)
	}
	err = pdf.Rectangle(10, 30, 110, 230, "", 0, 0)
	if err != nil {
		panic(err)
	}

	pdf.SetXY(10, 255)
	err = pdf.Text(" pdf.Rectangle(10, 260, 310, 360, \"\", 5, 10)")
	if err != nil {
		panic(err)
	}
	err = pdf.Rectangle(10, 260, 310, 360, "", 5, 10)
	if err != nil {
		panic(err)
	}

	save("rectangle-basic")
}

// TestRectFromLowerLeft Draw rectangle from bottom left corner.
func TestRectFromLowerLeft(t *testing.T) {
	var err error

	pdf.SetXY(20, 25)

	err = pdf.Text("RectFromLowerLeft(20, 200, 200, 170)")
	if err != nil {
		panic(err)
	}

	// Draw a rectangle with width of 200 and height of 170 with coordinates (20, 200) as the lower left corner.
	pdf.RectFromLowerLeft(20, 200, 200, 170)

	save("rectangle-lower-left")
}

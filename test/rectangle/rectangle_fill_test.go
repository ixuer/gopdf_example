package rectangle

import "testing"

// TestRectangleFill draw a rectangle with fill color.
func TestRectangleFill(t *testing.T) {
	var err error

	pdf.SetXY(30, 25)
	err = pdf.Text("pdf.Rectangle(30, 30, 310, 130, \"F\", 0, 0)")
	if err != nil {
		panic(err)
	}
	err = pdf.Rectangle(30, 30, 330, 130, "F", 0, 0)
	if err != nil {
		panic(err)
	}

	pdf.SetXY(30, 175)
	err = pdf.Text("pdf.SetFillColor(0, 142, 234)")
	pdf.SetXY(30, pdf.GetY()+10)
	err = pdf.Text("pdf.Rectangle(30, 190, 310, 290, \"DF\", 0, 0)")
	if err != nil {
		panic(err)
	}
	pdf.SetFillColor(0, 142, 234)
	err = pdf.Rectangle(30, 190, 330, 290, "DF", 0, 0)
	if err != nil {
		panic(err)
	}

	save("rectangle-fill")
}

package hyperlink

import (
	"testing"

	"github.com/signintech/gopdf"
)

func TestInternalHyperLink(t *testing.T) {
	var err error
	var anchorTop = "anchorTop"
	var anchorBottom = "anchorBottom"
	var topContent = "Click here to jump to the bottom of the page."
	var bottomContent = "This is the bottom of the page, back to top."
	var x1 float64 = 50
	var fontSize float64 = 20
	var yTop = pdf.MarginTop()
	var yBottom = gopdf.PageSizeA4.H - pdf.MarginBottom()

	err = pdf.SetFontSize(fontSize)
	if err != nil {
		panic(err)
	}

	w1, err := pdf.MeasureTextWidth(topContent)
	if err != nil {
		panic(err)
	}

	w2, err := pdf.MeasureTextWidth(topContent)
	if err != nil {
		panic(err)
	}

	// jump to bottom
	pdf.SetXY(x1, yTop+fontSize)
	pdf.SetAnchor(anchorTop)
	pdf.SetTextColor(0, 108, 214)
	err = pdf.Text(topContent)
	if err != nil {
		panic(err)
	}

	pdf.AddInternalLink(anchorBottom, x1, yTop, w1, fontSize)

	// back to up
	pdf.SetXY(x1, yBottom)
	pdf.SetAnchor(anchorBottom)
	err = pdf.Text(bottomContent)
	if err != nil {
		panic(err)
	}

	pdf.AddInternalLink(anchorTop, x1, yBottom-fontSize, w2, fontSize)

	save("hyperlink-internal")
}

package hyperlink

import (
	"testing"
)

func TestExternalHyperLink(t *testing.T) {
	var err error

	var content = "click to visit signintech/gopdf"
	var url = "https://github.com/signintech/gopdf"
	var x, y float64 = 50, 50
	var w, h float64
	h = 20

	err = pdf.SetFontSize(h)
	if err != nil {
		panic(err)
	}
	w, err = pdf.MeasureTextWidth(content)
	if err != nil {
		panic(err)
	}

	pdf.SetXY(x, y+h)
	err = pdf.Text(content)
	if err != nil {
		panic(err)
	}

	pdf.AddExternalLink(url, x, y, w, h)

	// set font color
	pdf.SetXY(x, y+h+100)
	pdf.SetTextColor(0, 108, 214)
	err = pdf.Text(content)
	if err != nil {
		panic(err)
	}

	pdf.AddExternalLink(url, x, y+200, w, h)

	// set font color and add underline
	pdf.SetXY(x, y+h+200)
	pdf.SetTextColor(0, 108, 214)
	err = pdf.Text(content)
	if err != nil {
		panic(err)
	}
	pdf.SetStrokeColor(0, 108, 214)
	pdf.Line(x, y+h+204, x+w, y+h+204)

	pdf.AddExternalLink(url, x, y+200, w, h)

	save("hyperlink-external")
}

package image

import (
	"testing"

	"github.com/signintech/gopdf"

	"gitee.com/ixuer/gopdf_example/utils"
)

// image crop
func TestImageCrop(t *testing.T) {

	var err error
	var x, y, x2 float64 = 20, 20, 120

	filePath := "../../assets/img/gopher03.png"

	imageWidth, imageHeight := utils.GetImageSize(filePath)
	w, h := float64(imageWidth)/4, float64(imageHeight)/4

	holder, err := gopdf.ImageHolderByPath(filePath)
	if err != nil {
		panic(err)
	}

	// origin image
	err = text(x, y+40, "origin --->")
	if err != nil {
		panic(err)
	}

	err = pdf.ImageByHolder(holder, x2, y, &gopdf.Rect{W: w, H: h})
	if err != nil {
		panic(err)
	}

	// crop
	err = text(x, y+h*2, "crop --->")
	if err != nil {
		panic(err)
	}

	option4 := gopdf.ImageOptions{
		DegreeAngle:    0,
		VerticalFlip:   false,
		HorizontalFlip: false,
		X:              x2,
		Y:              200,
		Rect: &gopdf.Rect{
			W: w,
			H: h,
		},
		Mask: nil,
		Crop: &gopdf.CropOptions{
			X:      0, // (X,Y) is the coordinates relative to at the top left of the image.
			Y:      0,
			Width:  w / 3,
			Height: h,
		},
		Transparency: nil,
	}

	err = pdf.ImageByHolderWithOptions(holder, option4)
	if err != nil {
		panic(err)
	}

	save("image-options-crop")
}

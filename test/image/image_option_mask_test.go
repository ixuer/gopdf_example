package image

import (
	"fmt"
	"testing"

	"github.com/signintech/gopdf"

	"gitee.com/ixuer/gopdf_example/utils"
)

func TestMask(t *testing.T) {
	var err error
	var x, y, x2 float64 = 20, 20, 120

	filePath := "../../assets/img/gopher03.png"

	imageWidth, imageHeight := utils.GetImageSize(filePath)
	w, h := float64(imageWidth)/4, float64(imageHeight)/4

	holder, err := gopdf.ImageHolderByPath(filePath)
	if err != nil {
		panic(err)
	}

	// origin image
	err = text(x, y+40, "origin --->")
	if err != nil {
		panic(err)
	}

	err = pdf.ImageByHolder(holder, x2, y, &gopdf.Rect{W: w, H: h})
	if err != nil {
		panic(err)
	}

	// add mask
	err = text(x, y+h*2, "mask --->")
	if err != nil {
		panic(err)
	}

	maskHolder, err := gopdf.ImageHolderByPath("../../assets/img/mask.png")
	if err != nil {
		panic(err)
	}

	fmt.Println("x2: ", x2, "\ny+1.5*h: ", y+1.5*h)
	maskOptions := &gopdf.MaskOptions{
		ImageOptions: gopdf.ImageOptions{
			X: x2,
			Y: y + 1.5*h,
			Rect: &gopdf.Rect{
				W: 100,
				H: 100,
			},
			Mask:         nil,
			Crop:         nil,
			Transparency: nil,
		},
		BBox:   nil,
		Holder: maskHolder,
	}

	option4 := gopdf.ImageOptions{
		X: x2,
		Y: y + 1.5*h,
		Rect: &gopdf.Rect{
			W: w,
			H: h,
		},
		Mask: maskOptions,
		Crop: nil,
		Transparency: &gopdf.Transparency{
			Alpha:         0.3,
			BlendModeType: "",
		},
	}

	err = pdf.ImageByHolderWithOptions(holder, option4)
	if err != nil {
		panic(err)
	}

	save("image-options-mask")
}

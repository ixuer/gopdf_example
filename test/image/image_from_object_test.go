package image

import (
	"image"
	"os"
	"testing"

	"github.com/signintech/gopdf"
)

// TestImageFromImageObject insert image by image.Image.
func TestImageFromImageObject(t *testing.T) {

	filePath := "../../assets/img/gopher02.png"

	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	img, _, err := image.Decode(f)
	if err != nil {
		panic(err)
	}

	var rect1 = &gopdf.Rect{
		W: 120,
		H: 120,
	}

	err = pdf.ImageFrom(img, 50, 100, rect1)
	if err != nil {
		panic(err)
	}

	save("image-from-object")
}

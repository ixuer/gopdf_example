package image

import (
	"testing"

	"github.com/signintech/gopdf"
)

// TestImageFlip Rotated by specified degree, or flip vertically or horizontally.
func TestImageFlip(t *testing.T) {
	filePath := "../../assets/img/gopher02.png"

	holder, err := gopdf.ImageHolderByPath(filePath)
	if err != nil {
		panic(err)
	}

	var x float64 = 20
	var x2 float64 = 120
	var y float64 = 10

	// origin image
	err = text(x, 60, "origin --->")
	if err != nil {
		panic(err)
	}

	err = pdf.ImageByHolder(holder, x2, y, &gopdf.Rect{W: 120, H: 120})
	if err != nil {
		panic(err)
	}

	// rotate 30°
	y += 200
	err = text(x, y, "degree angle = 30° --->")
	if err != nil {
		panic(err)
	}

	option1 := gopdf.ImageOptions{
		DegreeAngle: 30,
		X:           x2,
		Y:           y - 60,
		Rect: &gopdf.Rect{
			W: 120,
			H: 120,
		},
	}

	err = pdf.ImageByHolderWithOptions(holder, option1)
	if err != nil {
		panic(err)
	}

	// flip vertically
	y += 200
	err = text(x, y, "vertical flip --->")
	if err != nil {
		panic(err)
	}

	option2 := gopdf.ImageOptions{
		VerticalFlip: true,
		X:            x2,
		Y:            y - 60,
		Rect: &gopdf.Rect{
			W: 120,
			H: 120,
		},
	}

	err = pdf.ImageByHolderWithOptions(holder, option2)
	if err != nil {
		panic(err)
	}

	// flip horizontally
	y += 200
	err = text(x, y, "horizontal flip --->")
	if err != nil {
		panic(err)
	}

	option3 := gopdf.ImageOptions{
		HorizontalFlip: true,
		X:              x2,
		Y:              y - 60,
		Rect: &gopdf.Rect{
			W: 120,
			H: 120,
		},
	}

	err = pdf.ImageByHolderWithOptions(holder, option3)
	if err != nil {
		panic(err)
	}

	save("image-flip")
}

package image

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/signintech/gopdf"
)

func TestImageFromFile(t *testing.T) {
	filePath := "../../assets/img/gopher02.png"

	holder, err := gopdf.ImageHolderByPath(filePath)
	if err != nil {
		panic(err)
	}

	var rect1 = &gopdf.Rect{
		W: 120,
		H: 120,
	}

	err = pdf.ImageByHolder(holder, 50, 100, rect1)
	if err != nil {
		panic(err)
	}

	save("image-holder-file")
}

// TestImageFromBytes insert image by byte slice.
func TestImageFromBytes(t *testing.T) {
	filePath := "../../assets/img/gopher02.png"

	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	holder, err := gopdf.ImageHolderByBytes(bytes)
	if err != nil {
		panic(err)
	}

	var rect1 = &gopdf.Rect{
		W: 120,
		H: 120,
	}

	err = pdf.ImageByHolder(holder, 50, 100, rect1)
	if err != nil {
		panic(err)
	}

	save("image-holder-bytes")
}

// TestImageFromReader insert image by io.Reader.
func TestImageFromReader(t *testing.T) {
	filePath := "../../assets/img/gopher02.png"

	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	holder, err := gopdf.ImageHolderByReader(f)
	if err != nil {
		panic(err)
	}

	var rect1 = &gopdf.Rect{
		W: 120,
		H: 120,
	}

	err = pdf.ImageByHolder(holder, 50, 100, rect1)
	if err != nil {
		panic(err)
	}

	save("image-holder-reader")
}

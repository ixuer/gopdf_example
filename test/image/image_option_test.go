package image

/*
gopdf.ImageOptions

type ImageOptions struct {
	DegreeAngle    float64       // degree of angle
	VerticalFlip   bool          // vertical flip
	HorizontalFlip bool          // horizontal flip
	X              float64       // coordinate of x
	Y              float64       // coordinate of y
	Rect           *Rect         // image rectangle
	Mask           *MaskOptions  // image mask options
	Crop           *CropOptions  // image crop options. X,Y is the coordinates relative to at the top left of the image.
	Transparency   *Transparency // transparency options
}
*/

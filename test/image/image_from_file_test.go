package image

import (
	"testing"

	"github.com/signintech/gopdf"
)

func TestImageFromFilePath(t *testing.T) {
	var err error
	var imgPath1 = "../../assets/img/gopher01.jpg"
	var imgPath2 = "../../assets/img/gopher02.png"

	var rect1 = &gopdf.Rect{
		W: 354,
		H: 241,
	}

	var rect2 = &gopdf.Rect{
		W: 120,
		H: 120,
	}

	err = pdf.Image(imgPath1, 50, 50, rect1)
	if err != nil {
		panic(err)
	}

	err = pdf.Image(imgPath2, 50, 360, rect2)
	if err != nil {
		panic(err)
	}

	save("image-file")
}

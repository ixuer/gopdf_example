package line

import "testing"

// TestLine draw lines with different line style.
func TestLine(t *testing.T) {

	// default line style
	pdf.Line(20, 20, 320, 20)

	pdf.Line(20, 50, 350, 80)

	// default line style，line width is 3.
	pdf.SetLineWidth(3)
	pdf.Line(20, 100, 320, 120)

	// dashed line, line width is 2.
	pdf.SetLineType("dashed")
	pdf.SetLineWidth(2)
	pdf.Line(20, 150, 320, 150)

	// dotted line, line width is 1.
	pdf.SetLineType("dotted")
	pdf.SetLineWidth(1)
	pdf.Line(20, 180, 320, 180)

	// Bézier curve, which called 'pen tools' in PhotoShop.
	pdf.SetLineType("solid")
	pdf.Curve(20, 240, 120, 350, 300, 220, 100, 230, "")

	save("line")
}

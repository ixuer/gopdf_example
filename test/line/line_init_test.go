package line

import (
	"strconv"

	"github.com/signintech/gopdf"

	"gitee.com/ixuer/gopdf_example/utils"
)

var pdf = gopdf.GoPdf{}

var unit = 20

// init add and set fonts, add page with grid line.
func init() {
	var err error

	pdf.Start(gopdf.Config{
		Unit:     gopdf.UnitPT,
		PageSize: *gopdf.PageSizeA4,
	})

	err = pdf.AddTTFFont("Alibaba-PuHuiTi-Regular", "../../assets/ttf/Alibaba-PuHuiTi-Regular.ttf")
	if err != nil {
		panic(err)
	}

	err = pdf.SetFont("Alibaba-PuHuiTi-Regular", "", 14)
	if err != nil {
		panic(err)
	}

	addPage()
}

// addPage add a page with grid line.
func addPage() {
	pdf.AddPage()
	err := addGrid()
	if err != nil {
		panic(err)
	}
}

// addGrid add the grid line to show the coordinate of the page.
func addGrid() error {
	var err error

	transparency := gopdf.Transparency{
		Alpha:         0.2,
		BlendModeType: gopdf.Color,
	}

	pdf.SetLineType("dotted")

	pdf.SetLineWidth(0.5)

	w := gopdf.PageSizeA4.W
	h := gopdf.PageSizeA4.H

	err = pdf.SetFontSize(8)
	if err != nil {
		panic(err)
	}
	pdf.SetStrokeColor(255, 0, 0)
	pdf.SetTextColor(220, 220, 220)
	err = pdf.SetTransparency(transparency)
	if err != nil {
		return err
	}

	xs := make([]float64, 0)
	for x := unit; float64(x) < w; x += unit {
		xs = append(xs, float64(x))
	}

	ys := make([]float64, 0)
	for y := unit; float64(y) < h; y += unit {
		ys = append(ys, float64(y))
	}

	for _, x := range xs {
		pdf.SetX(x)
		err = pdf.Text(strconv.Itoa(int(x)))
		if err != nil {
			panic(err)
		}

		pdf.Line(x, 0, x, h)
	}

	for _, y := range ys {
		pdf.SetX(0)
		pdf.SetY(y)
		err = pdf.Text(strconv.Itoa(int(y)))
		if err != nil {
			panic(err)
		}
		pdf.Line(0, y, w, y)
	}

	pdf.SetXY(0, 20)

	pdf.ClearTransparency()
	pdf.SetTextColor(0, 0, 0)
	pdf.SetLineType("solid")
	pdf.SetLineWidth(1)
	pdf.SetStrokeColor(0, 0, 0)

	return nil
}

// save the file
func save(prefix string) {
	var err error

	fileName := utils.GenerateFileName(prefix, "pdf")

	err = pdf.WritePdf(fileName)
	if err != nil {
		panic(err)
	}
}

// text write the specified text at specified coordinate.
func text(x, y float64, text string) error {
	var err error
	pdf.SetXY(x, y)
	err = pdf.Text(text)
	if err != nil {
		return err
	}

	return nil
}

package font

import "testing"

func TestFont(t *testing.T) {
	var err error

	pdf.AddPage()

	// add ttf fonts
	err = pdf.AddTTFFont("Alibaba-PuHuiTi-Regular", "../../assets/ttf/Alibaba-PuHuiTi-Regular.ttf")
	if err != nil {
		panic(err)
	}

	err = pdf.AddTTFFont("Alibaba-PuHuiTi-Bold", "../../assets/ttf/Alibaba-PuHuiTi-Bold.ttf")
	if err != nil {
		panic(err)
	}

	// set font
	err = pdf.SetFont("Alibaba-PuHuiTi-Regular", "", 14)
	if err != nil {
		panic(err)
	}

	err = text(20, 20, "font-family: Alibaba-PuHuiTi-Regular, font-size: 14")
	if err != nil {
		panic(err)
	}

	// set font
	err = pdf.SetFont("Alibaba-PuHuiTi-Bold", "", 14)
	if err != nil {
		panic(err)
	}

	err = text(20, 50, "font-family: Alibaba-PuHuiTi-Bold, font-size: 14")
	if err != nil {
		panic(err)
	}

	save("font")

}

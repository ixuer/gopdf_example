## Go语言生成PDF文件

[English](./README.md) | [简体中文](./README_zh_CN.md)

使用[signintech/gopdf](https://github.com/signintech/gopdf)生成PDF文件。

#### 测试用例覆盖面：
1. 设置页面配置，如宽度和高度。
2. [test/font](./test/font): 添加ttf字体，并设置字体。
3. [test/text](./test/text): 写文本内容，包括长文本。
4. [test/line](./test/line): 画不同种类的线。
5. [test/oval](./test/oval): 画一个椭圆形。
6. [test/polygon](./test/polygon): 画一个多边形，像三角形，五边形。
7. [test/rectangle](./test/rectangle): 画一个矩形。
8. [test/image](./test/image): 插入图像，选项可选，如旋转指定角度、垂直或水平翻转、遮罩、透明度。
9. [test/hyperlink](./test/hyperlink): 添加内部或外部超链接。
10. [test/outline](./test/outline): 添加大纲（或称为书签），包括多级。
11. [test/compress](./test/compress): 设置压缩级别以压缩文件大小。
12. [test/protection](./test/protection): 为文档设置密码保护。
13. [test/import_tpl](./test/import_tpl): 从模板导入，模板可以是已有的PDF文件，可以实现添加封面。

#### Tips:
1. 测试用例位于`gopdf_examples/test/`目录下，每个用例都将通过自己的init函数初始化。

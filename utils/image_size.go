package utils

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"
)

// GetImageSize get image width and height.
// if error is "image: unknown format",
// import the image package of file suffix,
// like '_ "image/png"' or '_ "image/jpeg"'.
func GetImageSize(filePath string) (int, int) {
	f, err := os.Open(filePath)
	if err != nil {
		return 0, 0
	}

	config, _, err := image.DecodeConfig(f)
	if err != nil {
		return 0, 0
	}

	return config.Width, config.Height
}

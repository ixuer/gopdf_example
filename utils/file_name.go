package utils

import (
	"fmt"
	"strings"
	"time"
)

// GenerateFileName generate file name with datetime suffix.
func GenerateFileName(prefix, suffix string) string {
	ts := time.Now().Format("20060102150405.000000")
	ts = strings.Replace(ts, ".", "-", 1)
	return fmt.Sprintf("%s-%s.%s", prefix, ts, suffix)
}

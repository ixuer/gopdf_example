package utils

import "testing"

func TestGetImageSize(t *testing.T) {
	type args struct {
		filePath string
	}
	tests := []struct {
		name  string
		args  args
		want  int
		want1 int
	}{
		{
			name: "TestGetImageSize-1",
			args: args{
				filePath: "../assets/img/gopher02.png",
			},
			want:  120,
			want1: 120,
		},

		{
			name: "TestGetImageSize-2",
			args: args{
				filePath: "../assets/img/cover_1.png",
			},
			want:  1587,
			want1: 2381,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := GetImageSize(tt.args.filePath)
			if got != tt.want {
				t.Errorf("GetImageSize() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("GetImageSize() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

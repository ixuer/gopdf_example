package utils

import (
	"fmt"
	"testing"
)

func TestGenerateFileName(t *testing.T) {
	type args struct {
		prefix string
		suffix string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "GenerateFileName-1",
			args: args{
				prefix: "test1",
				suffix: "pdf",
			},
		},

		{
			name: "GenerateFileName-2",
			args: args{
				prefix: "test2",
				suffix: "jpg",
			},
		},

		{
			name: "GenerateFileName-3",
			args: args{
				prefix: "test3",
				suffix: "png",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GenerateFileName(tt.args.prefix, tt.args.suffix)
			fmt.Println("got: ", got)
		})
	}
}
